#include "theme.h"

namespace st {

std::vector<std::string> theme::terminal_coded_colors = {
    "#282828",
    "#FB4934",
    "#B8BB26",
    "#FABD2F",
    "#83A598",
    "#D3869B",
    "#8EC07C",
    "#FDF4C1",

    "#7C6F64",
    "#FB4933",
    "#B8BB26",
    "#FABD2F",
    "#83A598",
    "#D3869B",
    "#8EC07C",
    "#FFFFC8"};

std::unordered_map<theme::scope, theme::color_code> theme::scope_colors = {
    {theme::scope::foreground, theme::color_code{7}},
    {theme::scope::background, theme::color_code{0}},
    {theme::scope::cursor, theme::color_code{15}},
    {theme::scope::reverse_cursor, theme::color_code{0}}};

// font format:
// http://freedesktop.org/software/fontconfig/fontconfig-user.html
std::string theme::font = "Hack-11:antialias=true:autohint=true";

} // namespace st
