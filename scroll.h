#ifndef ST_SCROLL
#define ST_SCROLL

#include "types.h"

void kscrolldown(const Arg* a);
void kscrollup(const Arg* a);

#endif
