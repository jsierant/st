#pragma once
#include <exception>
#include <functional>
#include <string>
#include <string_view>

namespace st {

template <typename F, typename... A>
void xcall(std::string_view func_name, F &func, A... args) {
  if (!func(std::forward<A>(args)...)) {
    throw std::runtime_error(std::string("failed to call ") + func_name.data());
  }
}

} // namespace st
