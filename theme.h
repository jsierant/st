
#pragma once

#include <cstdint>
#include <exception>
#include <stdexcept>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

namespace st {
namespace detail {

template <typename K, typename V>
V get_value_or_throw(std::unordered_map<K, V> const &map, K const &key,
                     std::string_view msg) {
  auto item = map.find(key);
  if (item == std::end(map)) {
    throw std::runtime_error(msg.data());
  }
  return item->second;
};

} // namespace detail

class theme {
public:
  using color_code = std::uint8_t;
  static std::string get_color(color_code code) {
    if (code >= terminal_coded_colors.size()) {
      throw std::runtime_error(
          std::string("theme: unable to find color for code:") +
          std::to_string(code));
    }
    return terminal_coded_colors[code];
  }

  static std::size_t get_color_count() { return terminal_coded_colors.size(); }

  enum class scope { foreground, background, cursor, reverse_cursor };

  static color_code get_color_code(scope scope) {
    return detail::get_value_or_throw(
        scope_colors, scope,
        std::string("theme: unable to find color code for scope:") +
            std::to_string(static_cast<int>(scope)));
  }

  static std::string get_font() { return font; }

private:
  theme() = default;
  static std::vector<std::string> terminal_coded_colors;
  static std::unordered_map<scope, color_code> scope_colors;
  static std::string font;
};

} // namespace st
